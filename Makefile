IPFS_HASH_FILE := $(shell mktemp)
GIT_COMMIT ?= $(shell git log -1 --pretty=format:"%h")

GITLAB_PASSWORD:=$(shell vault kv get -field=GITLAB_PASSWORD secret/qm64/providers)

# Builds cURL option to interact with Gitlab API
ifneq ($(GITLAB_PASSWORD),)
_CURL_GITLAB_TOKEN_HEADER := -H  "PRIVATE-TOKEN: $$GITLAB_PASSWORD" --fail
else
_CURL_GITLAB_TOKEN_HEADER := --form "job-token=$$CI_JOB_TOKEN" --fail
endif
.EXPORT_ALL_VARIABLES:

-include makefiles/*.mk

clean_%:
	rm -rf $*

clean: clean_build clean_dist
.PHONY: clean

prepare_dir: clean_build
	mkdir -p build
	mkdir -p dist/apt
	mkdir -p dist/orion
	cp -aR ./static/* ./dist
.PHONY: prepare

dist: deb_ipfs_amd64 deb_ipfs-service deb_ipfs-gateway collect_deb_packages repository_apt