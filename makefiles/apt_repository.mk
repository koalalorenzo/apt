GPG_KEY_ID := 7BD7D7DD3E220E0EB7389AF39731AA2171096268

collect_deb_packages:
	mkdir -p dist/apt/
	# This will move al the debian packages built/downloaded into the directory
	# to be built together in a repository
	-cp build/*.deb dist/apt/
	-cp build/deb/*/*.deb dist/apt/
	-cp dist/orion/*.deb dist/apt/
	-cp static/orion/*.deb dist/apt/
.PHONY: collect_deb_packages

repository_apt:
	cd dist; \
	apt-ftparchive -c ../Releases.conf packages apt > apt/Packages ;\
	cat apt/Packages | gzip -9c > apt/Packages.gz ;\
	bzip2 -kf apt/Packages ; \
	apt-ftparchive -c ../Releases.conf release apt > apt/Release ; \
	gpg --yes -abs -u ${GPG_KEY_ID} -o apt/Release.gpg apt/Release
.PHONY: repository_apt
