# Builds cURL option to interact with Gitlab API
ifneq ($(GITLAB_PASSWORD),)
_CURL_GITLAB_TOKEN_HEADER ?= -H  "PRIVATE-TOKEN: $$GITLAB_PASSWORD" --fail
else
_CURL_GITLAB_TOKEN_HEADER ?= --form "job-token=$$CI_JOB_TOKEN" --fail
endif

# Extract versions and tags
ORION_TAG_STABLE ?= $(shell curl -s ${_CURL_GITLAB_TOKEN_HEADER} https://gitlab.com/api/v4/projects/8184971/repository/tags | jq '.[] | .name' -r | grep -v "latest" | grep -v "beta" | head -n1)
ORION_TAG_BETA ?= $(shell curl -s ${_CURL_GITLAB_TOKEN_HEADER} https://gitlab.com/api/v4/projects/8184971/repository/tags | jq '.[] | .name' -r | grep -v "latest" | grep "beta" | head -n1)

ARCH ?= amd64

download_orion_release_beta: prepare_dir
	curl ${_CURL_GITLAB_TOKEN_HEADER} \
		-L "https://gitlab.com/api/v4/projects/8184971/jobs/artifacts/${ORION_TAG_BETA}/download?job=release:beta" \
		-o build/orion_release_beta.zip
.PHONY: download_orion_release_beta

download_orion_release_stable: prepare_dir
	curl ${_CURL_GITLAB_TOKEN_HEADER} \
		-L "https://gitlab.com/api/v4/projects/8184971/jobs/artifacts/${ORION_TAG_STABLE}/download?job=release:stable" \
		-o build/orion_release_stable.zip
.PHONY: download_orion_release_stable

orion_release_%:
	$(MAKE) download_orion_release_$*
	mkdir -p build/orion_release_unzipped_$*
	unzip -o build/orion_release_$*.zip -d build/orion_release_unzipped_$* \
		'build/*.yml' \
		'build/*.zip' \
		'build/*.deb' \
		'build/*.rpm' \
		'build/*.exe' \
		'build/*.AppImage' \
		'build/*.exe.blockmap'
	# Moves files to the right position
	mv build/orion_release_unzipped_$*/build/* dist/orion/
	# Populate the latest-version file by channel
	for filename in `ls dist/orion/*.yml | grep -v mac`; do \
		cat $$filename | yq read - version > dist/orion/$$( echo $$filename | sed 's|dist/orion/||g' | sed 's|.yml||g' )-version ; \
	done